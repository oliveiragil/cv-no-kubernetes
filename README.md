# Teste Zenvia


### Dependencias
Você precisa ter instalado o docker no ambiente local.
Se ainda não tem, pode começar por [Aqui](https://docs.docker.com/get-docker/ "Documentação oficial").

### Clone esse repositório
    ```git clone https://oliveiragil@bitbucket.org/oliveiragil/cv-no-kubernetes.git```

### Conteúdo estático
Faça as alterações que achar pertinentes no HTML dentro de [cv-web](./cv-web).

### Criando uma nova imagem docker
Execute o script build.sh na raíz desse projeto.
Este script vai construir uma imagem e tentar executá-la em sua `maquina local` na `porta 80`.
Se tudo der certo, você poderá conferir abrindo este endereço em seu navegador:
`http://lvh.me:8080/Gilson_Oliveira.html`

### Upload para o registry
Desde que você tenha acesso a um registry, você pode fazer upload, mas o ideal é ter uma configuração adequada de CI/CD para execução de testes e deploy. (Entendo que isso não está no escopo do Desafio).
    Todo:   1 - Configurar CI/CD.

### Setup Kubernetes

Dentro do diretorio kubernetes foram incluidos os arquivos yaml para criação dos recursos no kubernetes. O serviço foi exposto utilizando o Traefik como Ingress Controller. 
    Todo:   1 - Configurar certificados e porta https.
            2 - Configurar Helm, charts e ChartMuseum.


### ACESSANDO O AMBIENTE

Utilizei recursos mínimos para subir o ambiente com o menor custo possível. Isso inclui o serviço de DNS gratuito [No-IP](https://www.noip.com/ "Free Managed DNS") 

O curriculo publicado pode ser acessado em: [http://gilson10.ddns.net/](http://gilson10.ddns.net/)